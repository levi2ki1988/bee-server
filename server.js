'use strict';
const BS = require('browser-sync').create('beeline');

const appDir = './app',
    index = `order.html`;


BS.init({
    server: appDir,
    baseDir: appDir,
    index: index,
    files: [`${appDir}/${index}`, `${appDir}/*.css`],
    browser: ['chrome', 'firefox'],
    open: ['local', 'ui']
});