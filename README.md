# Dev server beeline customs

1. install node.js via [Node.js](https://nodejs.org/en/)

2. Clone this project `git clone https://gitlab.com/levi2ki1988/bee-server.git $FOLDERNAME`

3. run `npm install`

4. Place beeline files at floder `./app`

5. run `npm run server`


## Watch this [video](https://drive.google.com/open?id=0B97vjJBdHmKiZ2hlUlNVT0dfbFU) for main options overview

## Cons & Pros
### +
1. Low resources usage
2. Easy-to-use and deployment
3. Share between team members in office
4. Use in other projects in small changes

### -
1. Require Node.js
2. Require any console or node ide plugin